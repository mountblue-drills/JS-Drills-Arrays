const carDetails = require("./inventory")

const oldCarsCount = carDetails.reduce(function(count, curr){
    if(curr.car_year<2000){
        count+=1
    }
    return count
},0)
// console.log(oldCarsCount)
module.exports= oldCarsCount